<?php

if (!function_exists('arrayize')) {

    function arrayize($value)
    {
        if (is_array($value)) {
            return $value;
        } elseif (is_scalar($value)) {
            return [$value];
        }

        throw new \Exception('Only scalar or array values are accepted.');
    }
}
