<?php
/**
 * User: Maros Jasan
 * Date: 29.1.2018
 * Time: 21:53
 */

namespace Dense\Baseraiser\Connection;

class Config
{
    /**
     * @var array
     */
    private static $config = [];

    /**
     * @var string
     */
    private static $default = 'default';

    /**
     * @var string
     */
    private static $env = 'prod';

    /**
     * Config constructor.
     */
    private function __construct()
    {
    }

    /**
     * Config clone.
     */
    private function __clone()
    {
    }

    /**
     * @param array $config
     * @param string $default
     * @param string $env
     * @return void
     */
    public static function configure(array $config, $default, $env)
    {
        self::$config = $config;
        self::$default = $default;
        self::$env = $env;
    }

    /**
     * @return string
     */
    public static function getDefaultDb()
    {
        return self::$default;
    }

    /**
     * @param string $database
     * @return array
     * @throws \Exception
     */
    public static function getInstanceConfig($database)
    {
        if (!isset(self::$config[$database])) {
            throw new \Exception('Invalid connection setting.');
        }

        return self::$config[$database];
    }

    /**
     * @return bool
     */
    public static function isProdEnv()
    {
        return (self::$env === 'prod');
    }
}
