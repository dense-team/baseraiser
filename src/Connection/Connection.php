<?php
/**
 * User: Maros Jasan
 * Date: 26.6.2016
 * Time: 21:15
 */

namespace Dense\Baseraiser\Connection;

use Doctrine\DBAL\DriverManager;

class Connection
{
    /**
     * @var array
     */
    private static $instances = [];

    /**
     * Connection constructor.
     */
    private function __construct()
    {
    }

    /**
     * Connection clone.
     */
    private function __clone()
    {
    }

    /**
     * @param string|null $database
     * @return \Doctrine\DBAL\Connection
     * @throws \Exception
     */
    public static function instance($database = null)
    {
        if (!$database) {
            $database = Config::getDefaultDb();
        }

        if (!isset(self::$instances[$database])) {
            $config = Config::getInstanceConfig($database);

            $params = Helper::mapConfigToParams($config);

            self::$instances[$database] = DriverManager::getConnection($params);
        }

        return self::$instances[$database];
    }

    /**
     * @param string|null $database
     * @return \Doctrine\DBAL\Query\QueryBuilder
     * @throws \Exception
     */
    public static function query($database = null)
    {
        return self::instance($database)
            ->createQueryBuilder();
    }
}
