<?php
/**
 * User: Maros Jasan
 * Date: 29.1.2018
 * Time: 21:02
 */

namespace Dense\Baseraiser\Connection;

class Helper
{
    /**
     * @param array $config
     * @return array
     */
    static public function mapConfigToParams(array $config)
    {
        return [
            'driver' => 'pdo_' . $config['driver'],
            'host' => $config['host'],
            'port' => $config['port'],
            'dbname' => $config['database'],
            'user' => $config['username'],
            'password' => $config['password'],
        ];
    }
}
