<?php
/**
 * User: Maros Jasan
 * Date: 2.7.2016
 * Time: 13:30
 */

namespace Dense\Baseraiser\Middleware;

use \Dense\Baseraiser\Profiler\Profiler;

class ProfilerMiddleware
{
    /**
     * @param $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, \Closure $next)
    {
        return $next($request);
    }

    /**
     * @param $request
     * @param $response
     * @return void
     */
    public function terminate($request, $response)
    {
        if (!$request->ajax()) {
            $profiler = Profiler::getInstance();
            $profiler->printToConsole();
        }
    }
}
