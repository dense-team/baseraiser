<?php

namespace Dense\Baseraiser\Providers;

use Illuminate\Support\ServiceProvider as BaseServiceProvider;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Config;

use Dense\Baseraiser\Connection\Config as ConnectionConfig;

class ServiceProvider extends BaseServiceProvider
{
    public function boot()
    {
    }

    public function register()
    {
        $config = Config::get('database.connections');
        $default = Config::get('database.default');
        $env = App::environment();

        ConnectionConfig::configure($config, $default, $env);
    }
}
