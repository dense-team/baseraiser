<?php
/**
 * User: Maros Jasan
 * Date: 20.1.2018
 * Time: 17:43
 */

namespace Dense\Baseraiser\Query;

class Helper
{
    /**
     * @param mixed $value
     * @return int
     */
    public static function getParamType($value)
    {
        $paramType = \PDO::PARAM_STR;

        if (is_int($value)) {
            $paramType = \PDO::PARAM_INT;
        } elseif (is_bool($value)) {
            $paramType = \PDO::PARAM_BOOL;
        } elseif (is_null($value)) {
            $paramType = \PDO::PARAM_NULL;
        }

        return $paramType;
    }
}
