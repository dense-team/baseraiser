<?php

namespace Dense\Baseraiser;

interface RepositoryInterface
{
    /**
     * @param int $id
     * @return \Traversable
     */
    public function find($id);

    /**
     * @return \Traversable
     */
    public function all();

    /**
     * @param int|null $limit
     * @param int|null $offset
     * @return \Traversable
     */
    public function load($limit = null, $offset = null);

    /**
     * @param object $model
     * @return object
     */
    public function save($model);

    /**
     * @param object $model
     * @return object
     */
    public function create($model);

    /**
     * @param object $model
     * @return object
     */
    public function modify($model);

    /**
     * @param object $model
     * @return object
     */
    public function remove($model);
}
