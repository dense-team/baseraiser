<?php
/**
 * User: Maros Jasan
 * Date: 3.7.2016
 * Time: 17:20
 */

namespace Dense\Baseraiser\Profiler;

class Profiler
{
    /**
     * @var \Dense\Baseraiser\Profiler\Profiler
     */
    private static $instance;

    /**
     * @var array
     */
    private $info = [];

    /**
     * Profiler constructor.
     */
    private function __construct()
    {
    }

    /**
     * Profiler clone.
     */
    private function __clone()
    {
    }

    /**
     * @return \Dense\Baseraiser\Profiler\Profiler
     */
    public static function getInstance()
    {
        if (static::$instance === null) {
            static::$instance = new static();
        }

        return static::$instance;
    }

    /**
     * @param int $time
     * @param string $query
     * @param array $binds
     * @return void
     */
    public static function profile($time, $query, array $binds)
    {
        $info = new ProfilerBag($time, $query, $binds);

        $profiler = Profiler::getInstance();
        $profiler->addInfo($info);
    }

    /**
     * @param \Dense\Baseraiser\Profiler\ProfilerBag $bag
     * @return $this
     */
    public function addInfo(ProfilerBag $bag)
    {
        $this->info[] = $bag;

        return $this;
    }

    /**
     * @return bool
     */
    public function hasInfo()
    {
        return !empty($this->info);
    }

    /**
     * @return void
     */
    public function printToConsole()
    {
        if ($this->hasInfo()) {
            echo '<script>';
            foreach ($this->info as $bag) {
                $bag->printToConsole();
            }
            echo '</script>';
        }
    }
}
