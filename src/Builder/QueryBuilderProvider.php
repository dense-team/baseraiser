<?php
/**
 * User: Maros Jasan
 * Date: 19.2.2017
 * Time: 18:48
 */

namespace Dense\Baseraiser\Builder;

use Dense\Baseraiser\Connection\Connection;
use Dense\Baseraiser\Query\Helper as QueryHelper;

trait QueryBuilderProvider
{
    /**
     * @var \Doctrine\DBAL\Connection
     */
    protected $connection;

    /**
     * @var \Doctrine\DBAL\Query\QueryBuilder
     */
    protected $builder;

    /**
     * @var string
     */
    protected $run = 'EXEC';

    /**
     * @return null
     */
    protected function identify()
    {
        return null;
    }

    /**
     * @return \Doctrine\DBAL\Connection
     * @throws \Exception
     */
    public function getConnection()
    {
        if (is_null($this->connection)) {
            $identifier = $this->identify();

            $this->connection = Connection::instance($identifier);
        }

        return $this->connection;
    }

    /**
     * @return \Doctrine\DBAL\Query\QueryBuilder
     * @throws \Exception
     */
    public function getBuilder()
    {
        if (is_null($this->builder)) {
            $this->builder = $this->getConnection()
                ->createQueryBuilder();
        }

        return $this->builder;
    }

    /**
     * @return $this
     */
    private function clearBuilder()
    {
        $this->builder = null;

        return $this;
    }

    /**
     * @return \Doctrine\DBAL\Driver\Statement|int
     * @throws \Exception
     */
    public function execute()
    {
        $builder = $this->getBuilder();

        $this->clearBuilder();

        if ($this->run === 'EXEC') {
            $result = $builder->execute();
        } else {
            $result = $builder->getSql();

            echo $result;
            exit();
        }

        return $result;
    }

    /**
     * @return $this
     */
    public function debug()
    {
        $this->run = 'DEBUG';

        return $this;
    }

    /**
     * @param array $columns
     * @return $this
     * @throws \Exception
     */
    public function setValuesForInsert(array $columns)
    {
        foreach ($columns as $column => $value) {
            $paramType = QueryHelper::getParamType($value);

            $query = ":{$column}";

            $this
                ->setValue($column, $query)
                ->setParameter($column, $value, $paramType);
        }

        return $this;
    }

    /**
     * @param array $columns
     * @return $this
     * @throws \Exception
     */
    public function setValuesForUpdate(array $columns)
    {
        foreach ($columns as $column => $value) {
            $paramType = QueryHelper::getParamType($value);

            $query = ":{$column}";

            $this
                ->set($column, $query)
                ->setParameter($column, $value, $paramType);
        }

        return $this;
    }

    /**
     * @param string $name
     * @param array $arguments
     * @return mixed
     * @throws \Exception
     */
    public function __call($name, array $arguments)
    {
        $builderClass = \Doctrine\DBAL\Query\QueryBuilder::class;
        if (method_exists($builderClass, $name)) {
            $builder = $this->getBuilder();

            $result = call_user_func_array([$builder, $name], $arguments);

            if (!($result instanceof $builderClass)) {
                return $result;
            }
        } else {
            $connectionClass = \Doctrine\DBAL\Connection::class;
            if (method_exists($connectionClass, $name)) {
                $connection = $this->getConnection();

                $result = call_user_func_array([$connection, $name], $arguments);

                if (!($result instanceof $connectionClass)) {
                    return $result;
                }
            }
        }

        return $this;
    }
}
