<?php
/**
 * User: Maros Jasan
 * Date: 7.2.2020
 * Time: 12:26
 */

namespace Dense\Baseraiser\Builder;

use Dense\Baseraiser\Query\Helper as QueryHelper;

trait ContainsGeoms
{
    /**
     * @var float
     */
    protected $simplifySelect = null;

    /**
     * @var float
     */
    protected $simplifyInsert = null;

    /**
     * @var float
     */
    protected $simplifyUpdate = null;

    /**
     * @var string
     */
    protected $forceDimmension;

    /**
     * @var float
     */
    public static $bufferSmoothing = 0.000002;

    /**
     * @param float $value
     * @return $this
     */
    public function simplifySelect($value)
    {
        $this->simplifySelect = $value;

        return $this;
    }

    /**
     * @param float $value
     * @return $this
     */
    public function simplifyInsert($value)
    {
        $this->simplifyInsert = $value;

        return $this;
    }

    /**
     * @param float $value
     * @return $this
     */
    public function simplifyUpdate($value)
    {
        $this->simplifyUpdate = $value;

        return $this;
    }

    /**
     * @return $this
     */
    public function force2D()
    {
        $this->forceDimmension = '2D';

        return $this;
    }

    /**
     * @return $this
     */
    public function force3D()
    {
        $this->forceDimmension = '3D';

        return $this;
    }

    /**
     * @return array
     */
    protected function geoms2D()
    {
        return [];
    }

    /**
     * @return array
     */
    protected function geoms3D()
    {
        return [];
    }

    /**
     * @return array
     */
    private function geoms()
    {
        return array_merge($this->geoms2D(), $this->geoms3D());
    }

    /**
     * @return bool
     */
    private function hasGeoms()
    {
        return !empty($this->geoms());
    }

    /**
     * @param string $column
     * @return bool
     */
    private function hasGeom($column)
    {
        return in_array($column, $this->geoms());
    }

    /**
     * @param string $column
     * @return bool
     */
    private function isGeom2D($column)
    {
        return in_array($column, $this->geoms2D());
    }

    /**
     * @param string $column
     * @return bool
     */
    private function isGeom3D($column)
    {
        return in_array($column, $this->geoms3D());
    }

    /**
     * @param string $column
     * @return bool
     */
    private function isGeom($column)
    {
        return in_array($column, $this->geoms());
    }

    /**
     * @return string
     */
    protected function row()
    {
        $selects = [];
        foreach ($this->geoms() as $geom) {
            $select = "{$geom}";

            if ($this->forceDimmension === '2D') {
                $select = "ST_Force2D({$select})";
            } elseif ($this->forceDimmension === '3D') {
                $select = "ST_Force3D({$select})";
            }

            if ($this->simplifySelect) {
                $select = "ST_Simplify({$select}, {$this->simplifySelect}, true)";
            }

            $selects[] = "ST_AsGeoJSON({$select}) AS {$geom}";
        }

        return "{$this->table()}.*, " . implode(', ', $selects);
    }

    /**
     * @param array $columns
     * @return $this
     * @throws \Exception
     */
    public function setValuesForInsert(array $columns)
    {
        $smoothing = self::$bufferSmoothing;

        foreach ($columns as $column => $value) {
            $paramType = QueryHelper::getParamType($value);

            $query = ":{$column}";

            if ($this->isGeom($column)) {
                $query = "ST_GeomFromGeoJSON({$query})";

                if ($this->simplifyInsert) {
                    $query = "ST_Simplify(ST_Buffer(ST_Buffer({$query}, {$smoothing}), -{$smoothing}), {$this->simplifyInsert}, true)";
                }

                if ($this->isGeom2D($column)) {
                    $query = "ST_Force2D({$query})";
                } elseif ($this->isGeom3D($column)) {
                    $query = "ST_Force3D({$query})";
                }

                $query = "ST_SetSRID({$query}, 4326)";
            }

            $this
                ->setValue($column, $query)
                ->setParameter($column, $value, $paramType);
        }

        return $this;
    }

    /**
     * @param array $columns
     * @return $this
     * @throws \Exception
     */
    public function setValuesForUpdate(array $columns)
    {
        $smoothing = self::$bufferSmoothing;

        foreach ($columns as $column => $value) {
            $paramType = QueryHelper::getParamType($value);

            $query = ":{$column}";

            if ($this->isGeom($column)) {
                $query = "ST_GeomFromGeoJSON({$query})";

                if ($this->simplifyUpdate) {
                    $query = "ST_Simplify(ST_Buffer(ST_Buffer({$query}, {$smoothing}), -{$smoothing}), {$this->simplifyUpdate}, true)";
                }

                if ($this->isGeom2D($column)) {
                    $query = "ST_Force2D({$query})";
                } elseif ($this->isGeom3D($column)) {
                    $query = "ST_Force3D({$query})";
                }

                $query = "ST_SetSRID({$query}, 4326)";
            }

            $this
                ->set($column, $query)
                ->setParameter($column, $value, $paramType);
        }

        return $this;
    }
}
