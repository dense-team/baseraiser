<?php
/**
 * User: Maros Jasan
 * Date: 7.2.2020
 * Time: 12:27
 */

namespace Dense\Baseraiser\Builder;

trait ContainsFilters
{
    /**
     * @param string $name
     * @param mixed $value
     * @return $this
     */
    public function addFilter($name, $value)
    {
        return $this;
    }

    /**
     * @param array $filters
     * @return $this
     */
    public function addFilters(array $filters)
    {
        foreach ($filters as $name => $value) {
            $this->addFilter($name, $value);
        }

        return $this;
    }
}
