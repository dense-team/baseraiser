<?php
/**
 * User: Maros Jasan
 * Date: 15.2.2016
 * Time: 7:55
 */

namespace Dense\Baseraiser\Table;

abstract class EntityTable extends SimpleTable
{
    /**
     * @return string
     */
    abstract protected function index();

    /**
     * @return string
     */
    protected function sequence()
    {
        return "{$this->table()}_{$this->index()}_seq";
    }

    /**
     * @param int|string $id
     * @return \Illuminate\Support\Collection
     * @throws \Exception
     */
    public function find($id)
    {
        $cols = $this->getQueryPart('select');
        if (!$cols) {
            $cols = $this->row();
        }

        $data = $this->select($cols)
            ->from($this->table())
            ->where("{$this->index()} = :id")
            ->setParameter('id', $id)
            ->execute()
            ->fetchAll();

        if (!$data) {
            throw new \Exception('Položka neexistuje');
        }

        return $this->getResult($data);
    }

    /**
     * @param object $object
     * @return object
     * @throws \Exception
     */
    public function save($object)
    {
        if ($this->getEntityId($object)) {
            return $this->modify($object);
        } else {
            return $this->create($object);
        }
    }

    /**
     * @param object $object
     * @return object
     * @throws \Exception
     */
    public function create($object)
    {
        $data = $object->toArray();
        $data = array_filter($data);

        unset($data[$this->index()]);

        $connecton = $this->getConnection();

        $connecton->beginTransaction();
        try {
            $this
                ->setValuesForInsert($data)
                ->insert($this->table())
                ->execute();

            $id = $connecton->lastInsertId($this->sequence());

            $connecton->commit();
        } catch (\Exception $e) {
            $connecton->rollBack();

            throw $e;
        }

        $this->setEntityId($object, $id);

        return $object;
    }

    /**
     * @param object $object
     * @return object
     * @throws \Exception
     */
    public function modify($object)
    {
        $id = $this->getEntityId($object);

        $data = $object->toArray();
        unset($data[$this->index()]);

        $this->setValuesForUpdate($data)
            ->update($this->table())
            ->where("{$this->index()} = :id")
            ->setParameter('id', $id)
            ->execute();

        return $object;
    }

    /**
     * @param object $object
     * @return object
     * @throws \Exception
     */
    public function remove($object)
    {
        $id = $this->getEntityId($object);

        $this->delete($this->table())
            ->where("{$this->index()} = :id")
            ->setParameter('id', $id)
            ->execute();

        return $object;
    }

    /**
     * @param object $object
     * @return int
     */
    protected function getEntityId($object)
    {
        if (is_object($object) && property_exists($object, $this->index())) {
            return $object->{$this->index()};
        }
    }

    /**
     * @param object $object
     * @param int $id
     * @return object
     */
    protected function setEntityId($object, $id)
    {
        if (is_object($object) && property_exists($object, $this->index())) {
            $object->{$this->index()} = $id;

            return $object;
        }
    }
}
