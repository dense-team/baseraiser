<?php
/**
 * User: Maros Jasan
 * Date: 15.2.2016
 * Time: 7:55
 */

namespace Dense\Baseraiser\Table;

use Dense\Baseraiser\Result\ResultTrait;
use Dense\Baseraiser\Builder\QueryBuilderProvider;

abstract class SimpleTable
{
    use ResultTrait, QueryBuilderProvider;

    /**
     * @return string
     */
    abstract protected function table();

    /**
     * SimpleTable constructor.
     * @param object $modelInstance
     */
    public function __construct($modelInstance)
    {
        $this->initResult($modelInstance);
    }

    /**
     * @return string
     */
    protected function row()
    {
        return "{$this->table()}.*";
    }

    /**
     * @return string
     */
    protected function cols()
    {
        return $this->row();
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function all()
    {
        $cols = $this->getQueryPart('select');
        if (!$cols) {
            $cols = $this->row();
        }

        $data = $this->select($cols)
            ->from($this->table())
            ->execute()
            ->fetchAll();

        return $this->getResult($data);
    }

    /**
     * @param int $limit
     * @param int $offset
     * @return \Illuminate\Support\Collection
     */
    public function load($limit, $offset)
    {
        if (!is_null($limit)) {
            $this->limit((int)$limit);
        }

        if (!is_null($offset)) {
            $this->offset((int)$offset);
        }

        $cols = $this->getQueryPart('select');
        if (!$cols) {
            $cols = $this->row();
        }

        $data = $this->select($cols)
            ->from($this->table())
            ->execute()
            ->fetchAll();

        return $this->getResult($data);
    }
}
