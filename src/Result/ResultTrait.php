<?php
/**
 * User: Maros Jasan
 * Date: 21.8.2016
 * Time: 10:24
 */

namespace Dense\Baseraiser\Result;

use Illuminate\Support\Collection;

trait ResultTrait
{
    /**
     * @var object|callable
     */
    private $prototype;

    /**
     * @param object|callable $prototype
     * @return $this
     */
    protected function initResult($prototype)
    {
        $this->prototype = $prototype;

        return $this;
    }

    /**
     * @param array $data
     * @return object
     */
    protected function getResultClone(array $data)
    {
        $prototype = $this->prototype;

        if (is_callable($prototype)) {
            $resultClone = $prototype($data);
        } else {
            $resultClone = clone $prototype;
            $resultClone->hydrate($data);
        }

        return $resultClone;
    }

    /**
     * @param array $data
     * @return \Illuminate\Support\Collection
     */
    public function getResult(array $data)
    {
        $index = null;
        if (method_exists($this, 'index')) {
            $index = $this->index();
        }

        $collection = new Collection();

        foreach ($data as $item) {
            $result = $this->getResultClone($item);

            if (!is_null($index) && array_key_exists($index, $item)) {
                $id = $item[$index];

                $collection->put($id, $result);
            } else {
                $collection->push($result);
            }
        }

        return $collection;
    }
}
