<?php
/**
 * User: Maros Jasan
 * Date: 19.1.2017
 * Time: 21:32
 */

use \PHPUnit\Framework\TestCase;

use Dense\Baseraiser\Connection\Connection;
use Dense\Baseraiser\Connection\Config as ConnectionConfig;

class ConnectionTest extends TestCase
{
    /**
     * return void
     */
    protected function configureConnection()
    {
        $config = [
            'pgsql' => [
                'driver'   => 'pgsql',
                'host'     => '127.0.0.1',
                'database' => 'test_db',
                'username' => 'test_username',
                'password' => 'test_password',
                'charset'  => 'utf8',
                'schema'   => 'public',
                'port'     => '5432',
                'prefix'   => '',
                'sslmode'  => 'prefer',
            ],
            'sqlsrv' => [
                'driver'   => 'sqlsrv',
                'host'     => '127.0.0.1',
                'database' => 'test_db',
                'username' => 'test_username',
                'password' => 'test_password',
                'charset'  => 'utf8',
                'port'     => '1542',
                'prefix'   => '',
            ],
        ];
        $database = 'pgsql';
        $env = 'prod';

        ConnectionConfig::configure($config, $database, $env);
    }

    /**
     * @param string|null $database
     * @return \Doctrine\DBAL\Connection
     * @throws Exception
     */
    protected function getConnection($database = null)
    {
        $this->configureConnection();

        return Connection::instance($database);
    }

    /**
     * @throws Exception
     */
    public function testPgsqlConnection()
    {
        $testConnection = $this->getConnection('pgsql');

        $driver = $testConnection->getDriver();

        $expected = \Doctrine\DBAL\Driver\PDOPgSql\Driver::class;

        $this->assertInstanceOf($expected, $driver);
    }
}
